package awssrv

import (
	"fmt"
	"io"
	"log"
	"sort"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type FileObject struct {
	Key  string `json:"key"`
	File string `json:"file"`
	Size int64  `json:"size"`
	Time string `json:"time"`
	Date string `json:"date"`
}

func (fo FileObject) String() string {
	return fmt.Sprintf("%s-%s", fo.Time, fo.Date)
}

type FileObjectList []*FileObject

type FileList struct {
	Files FileObjectList `json:"files"`
}

func (f FileObjectList) Len() int {
	return len(f)
}

func (f FileObjectList) Swap(i, j int) {
	f[i], f[j] = f[j], f[i]
}

func (f FileObjectList) Less(i, j int) bool {
	if f[i].Date == f[j].Date {
		return (f[i].Time < f[j].Time)
	} else {
		return (f[i].Date < f[j].Date)
	}
}

func GetFileIAM(region, bucketName, path string) (io.ReadCloser, int64, error) {
	awsconfig := &aws.Config{
		Region:           aws.String(region),
		Endpoint:         aws.String("s3.amazonaws.com"),
		S3ForcePathStyle: aws.Bool(true),
		LogLevel:         aws.LogLevel(0),
	}

	sess := session.New(awsconfig)
	svc := s3.New(sess)

	params := &s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(path),
	}

	resp, err := svc.GetObject(params)
	if err != nil {
		return nil, 0, err
	}

	// log.Println(resp)
	return resp.Body, *resp.ContentLength, nil
}

func GetFile(region, bucketName, path, keyId, secretKey, token string) (io.ReadCloser, int64, error) {
	creds := credentials.NewStaticCredentials(keyId, secretKey, token)

	if _, err := creds.Get(); err != nil {
		return nil, 0, err
	}

	awsconfig := &aws.Config{
		Region:           aws.String(region),
		Endpoint:         aws.String("s3.amazonaws.com"),
		S3ForcePathStyle: aws.Bool(true),
		Credentials:      creds,
		LogLevel:         aws.LogLevel(0),
	}

	sess := session.New(awsconfig)
	svc := s3.New(sess)

	params := &s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(path),
	}

	resp, err := svc.GetObject(params)
	if err != nil {
		return nil, 0, err
	}

	// log.Println(resp)
	return resp.Body, *resp.ContentLength, nil
}

func ListFolder(region, bucketName, path, keyId, secretKey, token string, iam bool) (FileList, error) {
	var f FileList
	var awsconfig *aws.Config
	if !iam {
		creds := credentials.NewStaticCredentials(keyId, secretKey, token)
		var f FileList

		if _, err := creds.Get(); err != nil {
			return f, err
		}

		awsconfig = &aws.Config{
			Region:           aws.String(region),
			Endpoint:         aws.String("s3.amazonaws.com"),
			S3ForcePathStyle: aws.Bool(true),
			Credentials:      creds,
			LogLevel:         aws.LogLevel(0),
		}
	} else {
		awsconfig = &aws.Config{
			Region:           aws.String(region),
			Endpoint:         aws.String("s3.amazonaws.com"),
			S3ForcePathStyle: aws.Bool(true),
			LogLevel:         aws.LogLevel(0),
		}
	}
	sess := session.New(awsconfig)
	svc := s3.New(sess)

	inparams := &s3.ListObjectsInput{
		Bucket:    aws.String(bucketName),
		Prefix:    aws.String(path),
		Delimiter: aws.String("/"),
	}

	err := svc.ListObjectsPages(inparams, func(p *s3.ListObjectsOutput, lastPage bool) bool {
		for _, o := range p.Contents {
			switch *o.StorageClass {
			case s3.ObjectStorageClassGlacier:
				log.Printf("GLACIER  %s\n", *o.Key)
				continue
			case s3.ObjectStorageClassReducedRedundancy:
				log.Printf("REDUCED  %s\n", *o.Key)
				continue
			case s3.ObjectStorageClassStandard:
				log.Printf("STANDARD %s\n", *o.Key)
			}
			loc, err := time.LoadLocation("America/New_York")
			if err != nil {
				log.Fatal(err)
			}
			keyString := *o.Key
			fileString := strings.TrimPrefix(keyString, *p.Prefix)
			lm := o.LastModified.In(loc)
			t := lm.Format("15:04:05")
			d := lm.Format("2006-01-02")

			f.Files = append(f.Files, &FileObject{keyString, fileString,
				*o.Size, t, d})
		}
		return !lastPage
	})
	if err != nil {
		log.Println(err)
	}
	log.Printf("Kept %d backup files.\n", len(f.Files))

	// log.Println(f)
	sort.Sort(sort.Reverse(f.Files))

	return f, nil
}

