# awssrv

## A convenience wrapper for some common-to-me actions

### Purpose
These are a couple of functions to access an Amazon S3 instance that I found myself using repeatedly for different projects so this repo just serves as a convenient way to get this functionality in new projects.

This is just a (thin) wrapper around the existing AWS SDK for Go.

Both functions take an AWS region, a bucket name, key ID and secret key for credentials, and an optional token. GetFile returns the body of the file in an io.ReadCloser, the content-length reported by Amazon, and an error. ListFolder returns a list of file-description objects containing the AWS key, filename, file size, and upload time and date.

Simple and ugly, but useful for me.