package main

import (
	"log"
	"os"
	"github.com/sarosamurai/awssrv/awssrv"
)

var keyId, secretKey string

func init() {
	keyId = os.Getenv("KEYID")
	secretKey = os.Getenv("SECRET_KEY")
}

func main() {
	region := "us-east-1"
	bucketName := "frazer-backup-eastcoast"
	path := "backups/40000/40000/40300/40398/"
	list, err := awssrv.ListFolder(region, bucketName, path, keyId, secretKey, "", false)
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("Got %d backups\n", len(list.Files))
}
